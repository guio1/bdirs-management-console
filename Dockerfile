FROM node:12.7-alpine AS base
WORKDIR /usr/src/app
COPY package.json ./
RUN npm install
COPY . .
RUN npm run clean
RUN npm run build


FROM nginx:1.17.1-alpine
COPY --from=base /usr/src/app/dist/bdirs-frontend /usr/share/nginx/html