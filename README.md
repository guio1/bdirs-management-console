<h3 align="center">
  <a href="https://github.com/bdirs/bdirs-web-frontend">
    BDIRS Frontend <span>(BDIRS BACHELOR DEGREE INFORMATION AND RECOMMENDATION SYSTEM)</span>
  </a>
</h3>

<p align="center">
  <a href="https://github.com/facebook/react-native/blob/master/LICENSE">
    <img src="https://img.shields.io/badge/license-MIT-blue.svg" alt="React Native is released under the MIT license." />
  </a>
  <a href="https://www.npmjs.org/package/react-native">
    <img src="https://badge.fury.io/js/react-native.svg" alt="Current npm package version." />
  </a>
  <a href="https://facebook.github.io/react-native/docs/contributing">
    <img src="https://img.shields.io/badge/PRs-welcome-brightgreen.svg" alt="PRs welcome!" />
  </a>
  <a href="https://codeclimate.com/github/bdirs/bdirs-web-frontend/maintainability"><img src="https://api.codeclimate.com/v1/badges/d34df41742df3c9437a2/maintainability" /></a>
  <a href="https://travis-ci.org/bdirs/bdirs-web-frontend">
  <img src="https://travis-ci.org/bdirs/bdirs-web-frontend.svg?branch=develop" />
  </a>
  <a href="https://codeclimate.com/github/bdirs/bdirs-web-frontend/test_coverage">
  <img src="https://api.codeclimate.com/v1/badges/dd505402cbc0e3afdcf9/test_coverage" />
  </a>
</p>

[![Netlify Status](https://api.netlify.com/api/v1/badges/73b4f8b4-2981-452a-a73a-3137cd5ac6ea/deploy-status)](https://app.netlify.com/sites/bdirs-frontend/deploys)

###### Background
This platform helps to address an issue with applying and getting the right degree program to to at
university more so public universities in Uganda.
Choosing the right program to do at university can help one get their dream job as well do something they love.

###### Project dependencies
- Angular 8
- Angular material 
- Jasmine & Karma


##### Setup 
- clone project using `git clone `
- cd into `bdirs-frontend`
- install dependencies using yarn by running `yarn` in the terminal or `npm install`
- Serve application using `ng serve`

#### Docker Setup 

- Build image by running `docker build -t bdirs-client .`

- Start container by running `docker run --name bdirs-client-container -d -p 8080:80 bdir-front-client`

- Access the application on `localhost:8080`

### Mentions
<a href="https://www.vecteezy.com/free-vector/career">Career Vectors by Vecteezy</a>