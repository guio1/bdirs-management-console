export const environment = {
  production: true,
  authUrl: 'https://bdirs-auth.herokuapp.com/api/v1/',
  betaUrl: 'https://us-central1-bdirs-9c7c0.cloudfunctions.net/'
};
