import { createAction } from '@ngrx/store';

export const startLoaderActionType = 'START_LOADER';

export const stopLoaderActionType = 'STOP_LOADER';

export const startLoaderAction = createAction(startLoaderActionType);

export const stopLoaderAction = createAction(stopLoaderActionType);
