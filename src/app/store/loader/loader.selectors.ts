import { createSelector } from '@ngrx/store';
import { LoaderState } from '@store/loader/loader.reducer';

export interface AppState {
  loading: LoaderState;
}

export const selectLoading = createSelector((state: AppState) => state.loading,
  (state: LoaderState) => state.loading);
