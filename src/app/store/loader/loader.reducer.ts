import {Action, createReducer, on} from '@ngrx/store';
import * as loaderActions from './loader.actions';

export  interface LoaderState {
  loading: boolean;
}

const _loadingReducer = createReducer<LoaderState>(
  {loading: false},
  on(loaderActions.startLoaderAction, (state => ({...state, loading: true}))),
  on(loaderActions.stopLoaderAction, (state => ({...state, loading: false})))
  );

export function loadingReducer(state: LoaderState, action: Action) {
  return _loadingReducer(state, action);

}
