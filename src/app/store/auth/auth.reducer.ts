import {Action, createReducer, on} from '@ngrx/store';
import * as userActions from './auth.actions';
import { User } from '@core/models/user.model';

export interface AuthState {
    user: User;
    isAuthenticated: boolean;
}

export const initialState: AuthState = {
    isAuthenticated: false,
    user: {}
};

const _authReducer = createReducer(
    initialState,
    on(userActions.setAuthenticationSuccess, (state, user: User ) => ({...state, user, isAuthenticated: true })),
    on(userActions.setLogoutUser, (state) => ({...state, isAuthenticated: false, user: {}}))
    );

export  function authreducer(state, actions) {
    return _authReducer(state, actions);
}
