import {createAction, props} from '@ngrx/store';
import {User} from '@core/models/user.model';

export const setAuthenticationSuccess = createAction('AUTHENICATION_SUCCESS', props<User>());

export const setLogoutUser = createAction('LOGOUT_SUCCESS');
