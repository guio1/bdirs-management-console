import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { RouterTestingModule } from '@angular/router/testing';
import { routes } from 'src/app/app-routing.module';
import { AuthenticationService, LoginResponse } from '@core/services/authentication.service';
import { MockSAuthenticationService } from '@core/guards/auth.guard.spec';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { MatSnackBar, MatSnackBarModule } from '@angular/material';
import {  OVERLAY_PROVIDERS } from '@angular/cdk/overlay';
import { AngularMaterialModule } from 'src/app/angular-material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { of } from 'rxjs';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let authService: AuthenticationService;
  let store: MockStore;
  let snackBar: MatSnackBar;

  const loginRespone: LoginResponse = {
    data: {
      access_token: 'token',
      id: 1,
      role: 'admin',
      username: 'username',
      email: 'mail@test.com'
    },
    success: true,
    message: 'Login Success'
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      imports: [RouterTestingModule.withRoutes(routes),
        AngularMaterialModule, ReactiveFormsModule, BrowserAnimationsModule, NoopAnimationsModule],
      providers: [{provide: AuthenticationService, useValue: MockSAuthenticationService},
        provideMockStore({initialState: {loading: false}}),
        MatSnackBar, MatSnackBarModule, OVERLAY_PROVIDERS]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    authService = TestBed.inject(AuthenticationService);
    store = TestBed.inject(MockStore);
    snackBar = TestBed.inject(MatSnackBar);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call loginService on login', () => {
    authService.loginUser = jasmine.createSpy().and.returnValue(of(loginRespone));
    component.login();
    expect(authService.loginUser).toHaveBeenCalled();
  });

  it('should dispatch loader action on login', () => {
    authService.loginUser = jasmine.createSpy().and.returnValue(of(loginRespone));
    store.dispatch = jasmine.createSpy();
    component.login();
    expect(store.dispatch).toHaveBeenCalled();
  });

  it('should set token on login success', () => {
    authService.loginUser = jasmine.createSpy().and.returnValue(of(loginRespone));
    localStorage.setItem = jasmine.createSpy();
    component.login();
    expect(localStorage.setItem).toHaveBeenCalled();
  });

  it('should reset form on login success', () => {
    authService.loginUser = jasmine.createSpy().and.returnValue(of(loginRespone));
    component.loginForm.reset = jasmine.createSpy();
    component.login();
    expect(component.loginForm.reset).toHaveBeenCalled();
    expect(component.loginForm.value.email).toEqual('');
  });

  it('should open snackbar on login success', () => {
    authService.loginUser = jasmine.createSpy().and.returnValue(of(loginRespone));
    snackBar.open = jasmine.createSpy();
    component.login();
    expect(snackBar.open).toHaveBeenCalled();
  });

});
