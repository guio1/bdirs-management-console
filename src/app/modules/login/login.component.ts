import { Component, OnInit, } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { MatSnackBar } from '@angular/material/snack-bar';
import { startLoaderAction, stopLoaderAction } from '@store/loader/loader.actions';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { selectLoading } from '@store/index';
import { AuthenticationService } from '@core/services/authentication.service';
import { setAuthenticationSuccess } from '@store/auth/auth.actions';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  $loading = this.store.select(selectLoading);
  hide = true;
  loginForm: FormGroup = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required, Validators.minLength(6)]),
    rememberMe: new FormControl(false)
  });

  constructor(private store: Store,
              private authService: AuthenticationService,
              private snackbar: MatSnackBar,
              private router: Router
  ) { }

  ngOnInit(): void {
  }

  login() {
    this.store.dispatch(startLoaderAction());
    const { email, password } = this.loginForm.value;
    this.authService.loginUser({ email, password }).subscribe(
      ({message, data: {access_token, username, email: userEmail, id, } }) => {
        this.store.dispatch(stopLoaderAction());
        this.loginForm.reset();
        localStorage.setItem('token', access_token);
        this.store.dispatch(setAuthenticationSuccess({username, email: userEmail, id}));
        this.snackbar.open(message || 'Login successful', 'Okay',
          { panelClass: 'notif-success', duration: 1000, verticalPosition: 'top', horizontalPosition: 'right'});
        this.router.navigate(['dashboard/home']);
      },
      ( {error} ) => {
        if (error.errors) {
          error.forEach((e: string) => {
            this.snackbar.open(e);
          });
        } else {
          this.snackbar.open(error.message || 'Invalid Login', 'Okay',
          { panelClass: 'notif-error', duration: 1000, verticalPosition: 'top', horizontalPosition: 'right' });
        }

        this.store.dispatch(stopLoaderAction());
      }
    );
  }

}
