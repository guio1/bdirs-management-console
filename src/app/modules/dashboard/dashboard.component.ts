import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { AuthState } from '@store/auth/auth.reducer';
import { Router } from '@angular/router';
import { setLogoutUser } from '@store/auth/auth.actions';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  $user = this.store.select('auth');
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(private breakpointObserver: BreakpointObserver,
              private store: Store<{auth: AuthState}>, private router: Router) {}

  ngOnInit() {

  }

  logout(): void {
    localStorage.removeItem('token');
    this.store.dispatch(setLogoutUser());
    this.router.navigate(['']);
  }

}
