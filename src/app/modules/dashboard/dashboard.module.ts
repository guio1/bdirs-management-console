import { NgModule } from '@angular/core';
import { HomeComponent } from './home/home.component';
import {DashboardRoutingModule} from '@modules/dashboard/dashboard-routing.module';
import { CommonModule } from '@angular/common';
import { AngularMaterialModule } from 'src/app/angular-material.module';
import {_MatMenuDirectivesModule, MatCardModule, MatGridListModule, MatIconModule, MatMenuModule} from '@angular/material';



@NgModule({
  declarations: [HomeComponent],
  imports: [
    DashboardRoutingModule,
    CommonModule,
    AngularMaterialModule
  ]
})
export class DashboardModule { }
