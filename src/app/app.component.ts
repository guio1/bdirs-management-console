import { Component, OnInit  } from '@angular/core';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs';
import {selectLoading} from '@store/index';
import { Router } from '@angular/router';
import { AuthenticationService } from '@core/services/authentication.service';
import { setAuthenticationSuccess } from '@store/auth/auth.actions';
import { startLoaderAction, stopLoaderAction } from '@store/loader/loader.actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit  {
  loading = false;
  $loading: Observable<boolean> = this.store.select(selectLoading);

  constructor(private store: Store<{}>,
              private router: Router,
              private authService: AuthenticationService
    ) {

  }

  ngOnInit(): void {
    const loggedIn = this.authService.isLoggedIn();
    if (loggedIn) {this.router.navigate(['dashboard/home']); }

  }


}
