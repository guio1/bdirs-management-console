import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Role } from '@core/models/user.model';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AuthState } from '@store/auth/auth.reducer';
import { Store } from '@ngrx/store';
import { startLoaderAction, stopLoaderAction } from '@store/loader/loader.actions';
import { setAuthenticationSuccess } from '@store/auth/auth.actions';

export interface LoginResponse {
  data: {
    username: string,
    role: Role,
    email: string,
    id: number,
    access_token: string;
  };
  success: boolean;
  message: string;
}

export interface ProfileResponse {
  data: {
    username: string,
    role: Role,
    email: string,
    id: number,
    uuid?: string
  };
  success: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private http: HttpClient, private router: Router, private store: Store<{auth: AuthState}>) { }

  loginUser({ email, password }: { email: string, password: string }): Observable<LoginResponse> {
    const response = this.http.post<LoginResponse>(`${environment.authUrl}users/login`, { username: email, password });
    return response;

  }

  getUser(): Observable<ProfileResponse> {
    const response = this.http.get<ProfileResponse>(`${environment.authUrl}users/profile/me`);
    return response;

  }

  getToken(): string | null {
    return localStorage.getItem('token');
  }

  isLoggedIn(): Observable<boolean> | boolean {
    if (!this.getToken()) {
      return false;
    }
    this.store.dispatch(startLoaderAction());
    return this.getUser().pipe(map((response) => {
      this.store.dispatch(stopLoaderAction());
      if (response.success) {
        const{data: {email, username, id, role}} = response;
        this.store.dispatch(setAuthenticationSuccess({email, username, id, role}));
        return true;
       }
      const params = {url: this.router.url} ;
      this.router.navigate(['/'], {queryParams: params});
      return false;
    }));
  }
}
