import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import { provideMockStore, MockStore } from '@ngrx/store/testing';

import { AuthenticationService, LoginResponse, ProfileResponse } from '../authentication.service';
import { routes } from 'src/app/app-routing.module';
import { of } from 'rxjs';
import { environment } from 'src/environments/environment';

describe('AuthenticationService', () => {
  const response: LoginResponse = {
    data: {
      id: 1,
      access_token: 'token',
      role: 'admin',
      username: 'username',
      email: 'email@test.com'
    },
    success: true,
    message: 'Login Success'
  };

  const profileResponse: ProfileResponse = {
    data: {
      id: 1,
      role: 'admin',
      username: 'username',
      email: 'email@test.com'
    },
    success: true
  };
  let service: AuthenticationService;
  let httpMock: HttpTestingController;
  let store: MockStore;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes(routes)],
      providers: [provideMockStore({}), AuthenticationService]
    });
    service = TestBed.inject(AuthenticationService);
    store = TestBed.inject(MockStore);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should should return loginResponse', () => {
    let loginRes: LoginResponse;
    service.loginUser({email: 'afff', password: 'ssasasa'}).subscribe((res) => loginRes = res);
    httpMock.expectOne(`${environment.authUrl}users/login`).flush(response);
    expect(loginRes).toEqual(response);
  });

  it('should get profile', () => {
    let profileRes: ProfileResponse;
    service.getUser().subscribe(res => profileRes = res);
    const req = httpMock.expectOne(`${environment.authUrl}users/profile/me`);
    req.flush(profileResponse);
    expect(profileRes).toEqual(profileResponse);
  });

  it('should call localStorage', () => {
    localStorage.getItem = jasmine.createSpy();
    service.getToken();
    expect(localStorage.getItem).toHaveBeenCalled();
  });

  it('should call getToken on checking if user is logged in', () => {
    service.getToken = jasmine.createSpy();
    service.isLoggedIn();
    expect(service.getToken).toHaveBeenCalled();
  });

  it('should call get user if token exists', () => {
    store.dispatch = jasmine.createSpy();
    service.getToken = jasmine.createSpy().and.returnValue('token');
    service.getUser = jasmine.createSpy().and.returnValue(of(profileResponse));
    const loggedIn = service.isLoggedIn();
    expect(service.getUser).toHaveBeenCalled();
    expect(loggedIn).toBeTruthy();
    expect(store.dispatch).toHaveBeenCalledTimes(1);
  });

});
