import { TestBed } from '@angular/core/testing';

import { AuthGuard } from './auth.guard';
import { AuthenticationService, LoginResponse } from '@core/services/authentication.service';
import { of, Observable } from 'rxjs';

export const loginRespone: LoginResponse = {
  data: {
    access_token: 'token',
    id: 1,
    role: 'admin',
    username: 'username',
    email: 'mail@test.com'
  },
  success: true,
  message: 'Login Success'
};
export class MockSAuthenticationService {
  isLoggedIn() {
    return false;
  }

  loginUser(): Observable<LoginResponse> {
    return of(loginRespone);
  }
}

describe('AuthGuard', () => {
  let guard: AuthGuard;
  let authService: AuthenticationService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{provide: AuthenticationService, useValue: MockSAuthenticationService}],
      imports: []
    });
    guard = TestBed.inject(AuthGuard);
    authService = TestBed.inject(AuthenticationService);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });

  it('should return logged in status', () => {
    authService.isLoggedIn = jasmine.createSpy();
    guard.canActivate();
    expect(authService.isLoggedIn).toHaveBeenCalled();
  });
});
