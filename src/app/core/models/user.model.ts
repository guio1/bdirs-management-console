export type Role = 'admin' | 'user';

export interface User {
    email?: string;
    id?: number;
    avatar?: string;
    role?: Role;
    username?: string;
    uuid?: string;
}
