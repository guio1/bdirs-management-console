import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthenticationService } from './services/authentication.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from './http/token.interceptor';
import { AuthGuard } from './guards/auth.guard';


@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [AuthenticationService, {
    provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true
  }, AuthGuard]
})
export class CoreModule { }
